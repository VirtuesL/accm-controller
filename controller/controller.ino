#include "MPU9250.h"

// an MPU9250 object with the MPU-9250 sensor on I2C bus 0 with address 0x68
MPU9250 IMU(Wire,0x68);
int status;
double dt = 0.010;

typedef union {
  float floatingPoint;
  byte binary[4];
  } binaryFloat;

void setup() {
  // serial to display data
  Serial.begin(115200);
  while(!Serial) {}

  // start communication with IMU 
  status = IMU.begin();
  if (status < 0) {
    while(1) {}
  }
  // setting the accelerometer full scale range to +/-8G 
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
  // setting the gyroscope full scale range to +/-500 deg/s
  IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(19);
}

void loop() {
  // read the sensor
  IMU.readSensor();

  // display the data
  double accX = IMU.getAccelX_mss();

  double accY = IMU.getAccelY_mss();

  double accZ = IMU.getAccelZ_mss();

  double gyroX = IMU.getGyroX_rads();

  double gyroY = IMU.getGyroY_rads();
  
  double gyroZ = IMU.getGyroZ_rads();

  binaryFloat R1; R1.floatingPoint = atan2(accY, accZ) /** RAD_TO_DEG*/;
  binaryFloat P1; P1.floatingPoint = atan2(-accX , sqrt(accY * accY + accZ * accZ)) /** RAD_TO_DEG*/;

  
  double gyroXrate = gyroX / 131.0; // Convert to deg/s
  double gyroYrate = gyroY / 131.0; // Convert to deg/s

  binaryFloat R2; R2.floatingPoint = 0.90 * (R2.floatingPoint + gyroXrate * dt) + 0.1 * R1.floatingPoint; // Calculate the angle using a Complimentary filter
  binaryFloat P2; P2.floatingPoint = 0.90 * (P2.floatingPoint + gyroYrate * dt) + 0.1 * P1.floatingPoint;

  Serial.write(R1.binary, 4);
  Serial.write(R2.binary, 4);
  Serial.write(P1.binary, 4);
  Serial.write(P2.binary, 4);
  delay(10);
}
