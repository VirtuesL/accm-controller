#include "MPU9250.h"
#include "ESP8266WiFi.h"
#include "DNSServer.h"
#include "ESP8266WebServer.h"
#include "WiFiManager.h"
#include "WiFiUdp.h"

WiFiUDP Udp;
MPU9250 IMU(Wire,0x68);
int status;
uint32_t timer;
static float Quaternion[] { 1, 0, 0, 0 };
float Kp = 0.033; /*manually tuned value*/
float dt = 0.499; /*idk this just works*/


void setup() {
  pinMode(13,INPUT_PULLUP);
  Serial.begin(115200);
  status = IMU.begin();
  if (status < 0) {
    while (1) {}
  }
  WiFiManager wman;
  wman.autoConnect();
  Serial.println("connected...yeey :)");
  // setting the accelerometer full scale range to +/-8G
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
  // setting the gyroscope full scale range to +/-500 deg/s
  IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(19);
}

void loop() {
  Udp.beginPacket(IPAddress(192,168,1,248),3400);
  // read the sensor
  float button = digitalRead(13);
  IMU.readSensor();

  // display the data
  double ax = IMU.getAccelX_mss();
  double ay = IMU.getAccelY_mss();
  double az = IMU.getAccelZ_mss();

  double gx = IMU.getGyroX_rads();
  double gy = IMU.getGyroY_rads();
  double gz = IMU.getGyroZ_rads();

  double mx = IMU.getMagX_uT();
  double my = IMU.getMagY_uT();
  double mz = IMU.getMagZ_uT();
  

  float q1 = Quaternion[0], q2 = Quaternion[1], q3 = Quaternion[2], q4 = Quaternion[3];
  float norm;
  float hx,hy,bx,bz;
  float vx,vy,vz,wx,wy,wz;
  float ex, ey, ez;
  float pa,pb,pc;

  float q1q1 = q1*q1;
  float q1q2 = q1*q2;
  float q1q3 = q1*q3;
  float q1q4 = q1*q4;
  float q2q2 = q2*q2;
  float q2q3 = q2*q3;
  float q2q4 = q2*q4;
  float q3q3 = q3*q3;
  float q3q4 = q3*q4;
  float q4q4 = q4*q4;
  
  norm = (float)sqrt(ax*ax+ay*ay+az*az);
  if (norm == 0) return;
  norm = 1.0/norm;
  ax *= norm;
  ay *= norm;
  az *= norm;

  norm = (float)sqrt(mx*mx+my*my+mz*mz);
  if (norm == 0) return;
  norm = 1.0/norm;
  mx *= norm;
  my *= norm;
  mz *= norm;

  hx = 2 * mx * (0.5 - q3q3 - q4q4) + 2 * my * (q2q4 - q1q4) + 2 * mz * (q2q4 + q1q3);
  hy = 2 * mx * (q2q3 + q1q4) + 2 * my * (0.5 - q2q2 - q4q4) + 2 * mz * (q3q4 - q1q2);
  bx = (float)sqrt((hx*hx)+(hy*hy));
  bz = 2 * mx * (q2q4 - q1q3) + 2 * my * (q3q4 + q1q2) + 2 * mz * (0.5 - q2q2 - q3q3);

  vx = 2 * (q2q4 - q1q3);
  vy = 2 * (q1q2 + q3q4);
  vz = q1q1 - q2q2 - q3q3 + q4q4;

  wx = 2 * bx * (0.5 - q3q3 - q4q4) + 2 * bz * (q2q4 - q1q3);
  wy = 2 * bx * (q2q3 - q1q4) + 2 * bz * (q1q2 + q3q4);
  wz = 2 * bx * (q1q3 + q2q4) + 2 * bz * (0.5 - q2q2 - q3q3);  

  ex = (ay * vz - az * vy) + (my * wz - mz * wy);
  ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
  ez = (ax * vy - ay * vx) + (mx * wy - my * wx);

  gx += Kp * ex;
  gy += Kp * ey;
  gz += Kp * ez;

  pa = q2;
  pb = q3;
  pc = q4;

  q1 = q1 + (-q2*gx - q3*gy - q4*gz) * (0.5 - dt);
  q2 = pa + (q1*gx + pb*gz - pc*gy) * (0.5 - dt);
  q3 = pb + (q1*gy - pa*gz + pc*gx) * (0.5 - dt);
  q4 = pc + (q1*gz + pa*gy - pb*gx) * (0.5 - dt);

  norm = (float)sqrt(q1*q1 + q2*q2 + q3*q3);
  norm = 1.0/norm;
  Quaternion[0] = q1 * norm;
  Quaternion[1] = q2 * norm;
  Quaternion[2] = q3 * norm;
  Quaternion[3] = q4 * norm;

  double sinr_cosp = 2.0*(q1*q2+q3*q4);
  double cosr_cosp = 1.0 - 2.0*(q2*q2*q3*q3);
  double roll = atan2(sinr_cosp, cosr_cosp);

  double pitch;
  double sinp = 2.0 * (q1*q4-q2*q4);
  if(abs(sinp) >= 1.0){
    pitch = copysign(PI/2, sinp);
  }else{
    pitch = asin(sinp);
  }

  double siny_cosp = 2.0 * (q1*q4+q2*q3);
  double cosy_cosp = 1.0 - 2.0 * (q3*q3 + q4*q4);
  double yaw = atan2(siny_cosp,cosy_cosp);
  
  double posXYZ[] = {0,0,0};
  double eulerAngles[] = {yaw*180.0/PI, pitch*180.0/PI, roll*180.0/PI};

  char const * pos = reinterpret_cast<char *>(posXYZ);
  char const * eu = reinterpret_cast<char *>(eulerAngles);

  Udp.write(pos,sizeof(eulerAngles));
  Udp.write(eu, sizeof(posXYZ));

  int success = Udp.endPacket();
}