use std::net::UdpSocket;
use std::thread::spawn;
use std::str;

fn main() {
    println!("Establishing socket");
    let socket = match UdpSocket::bind("0.0.0.0:3400"){
        Ok(s) => {println!("{:?}",s);s},
        Err(e) => panic!(e)
    };
    let mut buf = [0u8; 64];
    loop {
        match socket.recv_from(&mut buf) {
            Ok((nbytes, _addr)) => {
                spawn(move || {
                    print!("Bytesize: {} ", nbytes);
                    print!("{}            \r", str::from_utf8(&buf[..nbytes]).expect("no"));
                });
            },
            Err(err) => println!("Erore {:?}", err)
        };
    }
}
