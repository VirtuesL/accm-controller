#ACCM Controller

This project is an attempt to make a cheap DIY Virtual Reality controller.
Currently this runs on a NodeMCU with an MPU-9250 for the hardware, and a RUST based server for the software side.

To run this yourself you would need a hardware device to do so, but the RUST server should work out of the box.

I will upload a video demonstration soon™