import serial, sys, glob, threading
from struct import unpack
from time import sleep
from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np
import socket

class Controller:
    def __init__(self):
        super(Controller, self).__init__()
        self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server.bind(("0.0.0.0",3400))
        self.arr = [0,0,0,0]
        self.fig, self.ax = plt.subplots(2,2, subplot_kw=dict(polar=True))
        self.cols = ['c', 'b', 'm', 'r']
        self.labels = ['roll', 'gyro roll', 'pitch', 'gyro pitch']
        self.running = True
        self.rdthr = threading.Thread(target=self.read_dat)
        self.rdthr.start()
        
        for (i,j), value in np.ndenumerate(self.ax):
            self.ax[i, j].set_rlim(0,1)

        self.fig.legend(loc='lower right')
        self.ln = [self.ax[i, j].plot([],[], self.cols[l], label=self.labels[l])[0] for l, ((i, j), n) in enumerate(np.ndenumerate(self.ax))]


    def read_dat(self):
        while self.running:
            buf = self.server.recvfrom(64)[0]
            print([float(i) for i in buf.split(b',')]," "*10, "\r", end="")
            self.arr = [float(i) for i in buf.split(b',')]
        self.ser.close()
        

    def update(self, i):
        for i, l in enumerate(self.ln):
            l.set_data((0, self.arr[i]), (0,1))
        return self.ln

    def get_arr(self):
        return unpack('4f', self.arr)

        
    def close(self, event):
        print('Window closed, exiting...')
        self.running = False
        exit(0)


    def start(self):
        self.ani = animation.FuncAnimation(self.fig, self.update, interval=1, blit=True)
        self.fig.canvas.mpl_connect('close_event', self.close)
        plt.show()

if __name__ == '__main__':
    cont = Controller()
    cont.start()
